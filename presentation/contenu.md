<!-- .slide: class="slide" -->
# Demandez le programme

1. Démo utilisateur
2. La petite histoire
3. La plateforme
4. Retour d'expérience  
5. Avenir

---

<!-- .slide: class="slide" -->
# Demo utilisateur
https://portail.innovation.insee.eu

----

Vidéo : https://levitt.fr/Onyxia.webm

----


<img src=".\images\onyxia1.png" width="70%"  height="90%" class="center">

----

<img src=".\images\onyxia2.png" width="80%"  height="90%" class="center">

----

<img src=".\images\onyxia3.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia4.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia5.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia6.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia7.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia8.png" width="80%" height="90%" class="center">

---

<!-- .slide: class="slide" -->
# La petite histoire

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Le décor
 > L'Insee collecte, produit, analyse et diffuse des informations sur l’économie et la société française.

- 5300 agents dont :
    - 850 enquêteurs
    - 2000 statisticiens (réguliers ou occasionnels)
    - 300 développeurs et exploitants


----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Le décor
Les évolutions récentes
- des sources (enquêtes, administratives et maintenant sources privées)
- sur la manière de faire de la statistique 
- sur la manière de faire de l'informatique

et une volonté affirmée d'**innover et d'être en première ligne sur les sources de données**

----

<!-- .slide: class="slide" -->
## Juste avant...
> Salut Nico, on aurait besoin d'un cluster ELK pour faire notre projet "DataDream", ça serait possible ?

... 6 mois plus tard, vous obtenez enfin les premiers accès... sans que ça ne marche forcément comme vous le souhaitez.

----

<!-- .slide: class="slide" -->
## Juste avant...
> "Mince, j'ai encore lancé un calcul trop costaud, mais je ne vois pas comment faire autrement avec ce paquet de données" 

... La ferme de calcul a saturé, la quinzaine d'utilisateurs qui partageaient le serveur ont perdu leur session et leurs calculs. 

----

<!-- .slide: class="slide" -->
## Printemps 2017 : l'étincelle
Le hackathon New Techniques and Technologies for Statistics d'Eurostat

... et surtout sa préparation !
<!-- mismatch between jobs and skills at regional level in the EU through the use of data
Lors de la préparation, un environnement technique s'est avéré nécessaire.
Ca tombait bien, il y avait du vieux matériel dans les placards.-->

----

<!-- .slide: class="slide" -->
## La période shadow IT :  mi 2017 - mi 2018

* Dès le départ, la plateforme lab était ouverte à tous
* Le bouche à oreille a fait son oeuvre...
* ... sous l'oeil bienveillant de la hiérarchie

----

<!-- .slide: class="slide" -->
## La légalisation : septembre 2018

Avec une réorganisation, les labs ont affirmé leur volonté d'offrir du service

----

<!-- .slide: class="slide" -->
## L'accélération : 2019

* Lancement d'un projet d'évolution de l'ingénierie logicielle
* Projet d'ouverture de la plateforme aux services statistiques ministériels, sponsorisé par les ministères économiques et des financiers

----

<!-- .slide: class="slide" -->
## Les facteurs clés de l'histoire
* La réorganisation et la création de 2 structures en charge de l'innovation 
* Les personnes
* Une autonomie
* Une vision
* Un laisser-faire bienveillant

---

<!-- .slide: class="chapter" -->
# La plateforme

----

<!-- .slide: class="slide" -->
## La plateforme innovation
Sa raison d'être : **une infrastructure en libre service sur des technos de datascience et ingénierie logicielle, pour des besoins d'innovation**

Aujourd'hui, la plateforme innovation est :
- **cloud privé** orienté **traitement de la donnée**
- une capacité de calcul importante
- une adoption importante malgré de faibles garanties de service

----

<!-- .slide: class="slide" -->
## Une définition du cloud - NIST (2011)
5 caractéristiques :
- service en **libre-service** à la demande
- **accessible** sur l'ensemble d'un réseau
- **mutualisation** des ressources
- rapidement **élastique**
- le service doit être **mesurable** (mesure et affichage de la consommation)

----

<!-- .slide: class="slide" -->
## Les services de la plateforme innovation
- un **portail utilisateur** 
- des **services de traitement de la donnée**
- de **l'hébergement (sans engagement de disponibilité) de services partagés/mutualisés**
- des **services d'ingénierie logicielle**
- des **ressources brutes**
- un **accès à des services collaboratifs**  

----

<!-- .slide: class="slide" -->
## Un service technique pour une démarche d'accompagnement ciblée
Conduire une **démarche plateforme** : apporter un outillage et des savoir-faire pour accompagner la transformation numérique du service statistique autour de 5 grandes orientations
- la collaboration
- la statistique reproductible
- l'open-source
- la datascience
- l'ingénierie logicielle

----

<!-- .slide: class="slide" -->
## Un service pour l'innovation, pas le fonctionnement standard

L'organisation des usages de la plateforme et des bonnes pratiques d'accès aux outils sont récentes.

Reposant sur peu de personnes (équipe de 3 personnes), actuellement l'offre est 
- **une politique de mises à jour féroce**
- **disponibilité de type best effort**
- **pas de gestion des données confidentielles**
- **relativement peu de documentation et d'accompagnement**

----

<!-- .slide: class="slide" -->
## Coûts :
* Environ 100 k€ de matériel
* 50 k€/an de licences
* 20 k€/an d'internet
* 1/2 ETP de gestion

---

<!-- .slide: class="slide" -->
# Retours d'expérience

----

<!-- .slide: class="slide" -->
## Des services partagés pour du travail collaboratif
* Des succès (trop ?)
    * Messagerie instantanée (RocketChat)
    * Services d'ingénierie logicielle (Gitlab, Nexus, Sonar)
    * Gestion de projet agile (Taiga)
    * des bricoles utiles (HackMD, Etherpad, Ethercalc)
* Des échecs : 
    * Réseau social d'entreprise (HumHub)
    * un Stackoverflow interne

----

<!-- .slide: class="slide" -->
## Des réutilisations rapides d'investissements partagés
Exemple de l'Open Source Routing Machine 
https://osrm-france.alpha.innovation.insee.eu

----

<!-- .slide: class="slide" -->
## Un support pour des hackathons
* Hackathon **Les Champs de Sirene**
* Hackathon **Elire** (applications du répertoire électoral unique)
* Hackathon **Plosh** (Linked open statistics)

----

<!-- .slide: class="slide" -->
## Un vecteur de modernisation des développements
* Facilitateur de l'accès et utilisation de Docker
* de l'intégration et déploiement continu
* des enjeux européens (ex : application ARC)

----

<!-- .slide: class="slide" -->
## Un espace de prototypage
* Scraping des prix de transport ferrovière
* Exploration de la base DVF
* Evolution du modèle macroéonomique Mesange 
* Projet AMI IA d'amélioration de l'identification de l'établissement employeur dans le recensement

---

<!-- .slide: class="slide" -->
# Les enjeux à venir

----

<!-- .slide: class="slide" -->
## Ouverture aux partenaires de l'Insee : 2020
* Un choix à faire sur l'hébergement
* Des adaptations techniques
* Une adhésion à tester 
* Un mode de fonctionnement à trouver

----

<!-- .slide: class="slide" -->
## La pérennisation de services validés
* Rentrer dans les priorités de ceux qui assurent le fonctionnement courant 
* Trouver un responsable **ouvrage** et **oeuvre**, en particulier sur les services transverses
* Savoir passer le relai

----

<!-- .slide: class="slide" -->
## Pérenniser la plateforme
* Le faire survivre à ses créateurs
* Conserver son agilité
* Conserver la saine tension entre la demande utilisateur et l'agilité forte 

----

<!-- .slide: class="slide" -->
## Ne pas négliger la résistance organisationnelle

Des Startups d'Etat à l'Etat plateforme, Pezziardi & Verdier

Changement des règles du jeu, notamment dans le domaine de l'innovation à la *beta.gouv.fr*
* **Distribution des savoirs** vs concentration du savoir
* **Personnalisation** vs standardisation (des produits et process)
* **Alignement des penseurs et faiseurs** vs subordination
* **Transparence, responsabilisation et contrôle a posteriori** vs contrôle a priori
