# Content of the presentation
Presentation of the Insee innovation platform for the MTES dataday (2019, November 18). 

:arrow_forward: [Slides](http://juliettes.gitlab.io/datadayMtes/#/)

For obtaining a printable PDF, open the following link in Chrome browser and print with PDFcreator.

:arrow_forward: [Printable version](http://juliettes.gitlab.io/datadayMtes/index.html?print-pdf#/)